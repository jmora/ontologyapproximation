import sys, os, os.path, re, sqlite3, datetime

'''tables = (['comparisonresults (ontology text, entailedsize numeric)', 
           'globalresults (ontology text, appxsize numeric, entailedsize numeric, timereasoning numeric, totaltime numeric)',
           'localresults (ontology text, appxsize numeric, entailedsize numeric, totaltime numeric)',
           'ontologies (ontology text, originalsize numeric)',
           'problems (ontology text, problem text)',
           'old expressiveness (ontology text, expressiveness text)',
           'characteristics (ontology text, expressiveness text, axioms numeric, concepts numeric, roles numeric, attributes numeric)',
           'niceontologies (ontology text)'])'''
shouldmathcal = False
generaltemplate = '''\\begin{table}[!h]
              \\setlength{\\tabcolsep}{3pt}
              \\centering
              \\adjustbox{max width=\\columnwidth, max totalheight=\\textheight, center}{
              \\begin{tabular}{%s}
              %s \\\\
              %s \\\\
              \\end{tabular}
              }
              \\caption{%s}
              \\label{%s}
              \\end{table}'''

tables = {'results':(('Results of both approximation algorithms.','tab:resultsextract'),
              (['ontology', 'original axioms', 'GA axioms', 'GA entails original (\\%)', 'LA axioms', 'LA entails GA (\\%)', 'GA time (s)', 'LA time (s)']),
              '''select nice.ontology, ontologies.originalsize, globalresults.appxsize, 100.0*globalresults.entailedsize/ontologies.originalsize, localresults.appxsize,
                   100.0*comparisonresults.entailedsize/globalresults.appxsize, globalresults.totaltime/1000, localresults.totaltime/1000
              from niceontologies as nice
              left outer join ontologies on ontologies.ontology = nice.ontology
              left outer join globalresults on nice.ontology = globalresults.ontology
              left outer join localresults on nice.ontology = localresults.ontology
              left outer join comparisonresults on nice.ontology = comparisonresults.ontology 
              order by ontologies.originalsize'''),
          'errors': (('Errors found in the tests.', 'errors'),
              ['ontology', 'problem'], 
              '''select problems.ontology, problems.problem from niceontologies join problems on niceontologies.ontology = problems.ontology order by problems.ontology'''),
          'averagepercentages': (('Average percentages','averagepercentages'),
              ['GA entails original', 'LA entails GA'],
              '''select avg(averages.original), avg(averages.global) from (select 100.0*globalresults.entailedsize/ontologies.originalsize as original,
                   100.0*comparisonresults.entailedsize/globalresults.appxsize as global
              from niceontologies as nice
              left outer join ontologies on ontologies.ontology = nice.ontology
              left outer join globalresults on nice.ontology = globalresults.ontology
              left outer join localresults on nice.ontology = localresults.ontology
              left outer join comparisonresults on nice.ontology = comparisonresults.ontology 
              order by ontologies.originalsize ) as averages'''),
          'noga':(('Entailment for ontologies not globally approximable', 'laentailment'),
                   ['ontology', 'original axioms', 'LA axioms', 'LA entails original (\\%)', 'LA time (s)'],
                   '''select nice.ontology, ontologies.originalsize, localresults.appxsize, 100.0*localresults.entailedsize/ontologies.originalsize, localresults.totaltime/1000
                   from niceontologies as nice
                   left outer join ontologies on ontologies.ontology = nice.ontology
                   left outer join localresults on nice.ontology = localresults.ontology
                   left outer join globalresults on nice.ontology = globalresults.ontology
                   where globalresults.entailedsize is null
                   order by ontologies.originalsize
                   '''),
          'characteristics':(('Characteristics of the ontologies.', 'ontologiescharacteristics'),
                  ['ontology', 'expressiveness', 'axioms', 'concepts', 'roles', 'attributes'],
                  '''select n.ontology, cx.expressiveness, o.originalsize, cx.concepts, cx.roles, cx.attributes
                     from niceontologies as n
                     left outer join characteristics as cx on n.ontology = cx.ontology
                     left outer join ontologies as o on o.ontology = n.ontology order by o.originalsize''')
          }
          
fixNone = lambda x: '--' if x is None else str(x).split('.')[0]

def mathcaller(l):
  r = list(l)
  if len(r) > 3:
    r[1] = '$\\mathcal{%s}$'%r[1]
  return r

def buildRows(db, query):
  for row in db.cursor().execute(query):
    yield '&'.join(map(fixNone, mathcaller(row) if shouldmathcal else row))

def makeTable(db, params, columns, query):
  columndef = ' '.join(('c' for e in columns))
  columnhead = '&'.join(('\\rotable{%s}'%e for e in columns))
  rows = '\\\\\n'.join((buildRows(db, query)))
  return generaltemplate%((columndef, columnhead, rows)+params)
  
if __name__ == "__main__":
  with sqlite3.connect('appxresults.sqlite3') as db:
    for t in tables:
      shouldmathcal = t == 'characteristics'
      with open(t + '.tex', 'w') as texfile:
        texfile.write(makeTable(db, tables[t][0], tables[t][1], tables[t][2]))
    
