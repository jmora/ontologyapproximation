import sys, os, os.path, re, sqlite3, datetime

'''tables = (['comparisonresults (ontology text, entailedsize numeric)', 
           'globalresults (ontology text, appxsize numeric, entailedsize numeric, timereasoning numeric, totaltime numeric)',
           'localresults (ontology text, appxsize numeric, entailedsize numeric, totaltime numeric)',
           'ontologies (ontology text, originalsize numeric)',
           'problems (ontology text, problem text)',
           'old expressiveness (ontology text, expressiveness text)',
           'characteristics (ontology text, expressiveness text, axioms numeric, concepts numeric, roles numeric, attributes numeric)',
           'niceontologies (ontology text)'])'''
    
relevantTimes = {'reasoner': ('Precomputing inferences', 'Precomputing hierarchies'), 'total': ('Initializing approximation', 'All done, getting axiom statistics')}
localname = lambda x : x.split('/')[-1].split('\\')[-1].split('.')[0].replace('_', ' ').replace('ontology','')
id = lambda x: x
fs = [localname, id, int, int, int, int]
apply = lambda t: t[0](t[1])

def cleanStep(s):
  r = s.split(':')
  if len(r) == 1:
    return (s, None)
  if r[1].isdigit():
    return (r[0], int(r[1]))
  return (r[0], ':'.join(r[1:]))

def getMilis(code, values):
  d = values[relevantTimes[code][1]] - values[relevantTimes[code][0]]
  return d.seconds * 1000 + d.microseconds / 1000
  
def processLine(l):
  r = {} # sorry for next line, but that's the way I like it.
  m = re.match(r"(?P<year>\d+)-(?P<month>\d+)-(?P<day>\d+) (?P<hour>\d+):(?P<minute>\d+):(?P<second>\d+),(?P<microsecond>\d+)[\w \.\[\]]*- (?P<step>[\w ,\.\d:]+)", l)
  if m is None:
    return r
  m = m.groupdict()
  params = ['year', 'month', 'day', 'hour', 'minute', 'second', 'microsecond']
  d = {}
  for p in params:
    d[p] = int(m[p])
  d['microsecond'] = d['microsecond'] * 1000 # Yes, I lied.
  (s, a) = cleanStep(m['step'])
  r[s] = a
  if a is None:
    r[s] = datetime.datetime(**d)
  return r
  
if __name__ == "__main__":
  with sqlite3.connect('appxresults.sqlite3') as db:
    sql = lambda s: db.cursor().execute(s)
    sql2 = lambda s, v: db.cursor().execute(s, v)
    #sql('drop table expressiveness')
    sql('drop table niceontologies')
    sql('drop table characteristics')
    sql('create table niceontologies (ontology text)')
    sql('create table characteristics (ontology text, expressiveness text, axioms numeric, concepts numeric, roles numeric, attributes numeric)')
    
    with open('ontoExpressiveness.txt','r') as oe:
      for vs in (tuple(map(apply, zip(fs, l.split(';')))) for l in oe):
        sql2('insert or replace into characteristics values (?,?,?,?,?,?)', vs)
    
    with open('ontologiesFinal.txt', 'r') as of:
      for v in ( localname(l.strip()) for l in of if len(l) > 2):
        sql2('insert or replace into niceontologies values (?)', (v,))
    
    values = {}
    for l in  (['2014-05-06 17:41:24,604 [main] INFO  eu.optique.api.component.omm.runners.ApproximatorRunner - Initializing approximation',
                '2014-05-06 17:41:27,723 [main] TRACE eu.optique.api.component.omm.approximators.OntologyLocalApproximator - Starting approximation',
                '2014-05-06 17:42:25,220 [main] TRACE eu.optique.api.component.omm.approximators.OntologyLocalApproximator - Done, saving',
                '2014-05-06 17:42:26,007 [main] INFO  eu.optique.api.component.omm.runners.ApproximatorRunner - All done, getting axiom statistics',
                '2014-05-06 17:42:27,438 [main] INFO  eu.optique.api.component.omm.runners.ApproximatorRunner - Original number of axioms: 37696',
                '2014-05-06 17:42:27,461 [main] INFO  eu.optique.api.component.omm.runners.ApproximatorRunner - Approximation number of axioms: 72172']):
      values.update(processLine(l))
    sql2('insert or replace into localresults values (?,?,?,?)',('galen full', values['Approximation number of axioms'], None, getMilis('total',values)))
    sql2('insert or replace into ontologies values (?,?)',('galen full', values['Original number of axioms']))
    
    values = {}
    for l in  (['2014-05-06 17:41:24,605 [main] INFO  eu.optique.api.component.omm.runners.ApproximatorRunner - Initializing approximation',
                '2014-05-06 17:41:31,172 [main] TRACE eu.optique.api.component.omm.approximators.OntologyLocalApproximator - Starting approximation',
                '2014-05-06 17:45:32,423 [main] TRACE eu.optique.api.component.omm.approximators.OntologyLocalApproximator - Done, saving',
                '2014-05-06 17:45:35,120 [main] INFO  eu.optique.api.component.omm.runners.ApproximatorRunner - All done, getting axiom statistics',
                '2014-05-06 17:45:38,824 [main] INFO  eu.optique.api.component.omm.runners.ApproximatorRunner - Original number of axioms: 73543',
                '2014-05-06 17:45:38,871 [main] INFO  eu.optique.api.component.omm.runners.ApproximatorRunner - Approximation number of axioms: 140629']):
      values.update(processLine(l))
    sql2('insert or replace into localresults values (?,?,?,?)',('SNOMED', values['Approximation number of axioms'], None, getMilis('total',values)))
    sql2('insert or replace into ontologies values (?,?)',('SNOMED', values['Original number of axioms']))
    
    values = {}
    for l in  (['2014-05-07 19:06:47,965 [main] INFO  eu.optique.api.component.omm.runners.ApproximatorRunner - Initializing approximation',
                '2014-05-07 19:06:51,124 [main] TRACE eu.optique.api.component.omm.approximators.OntologyLocalApproximator - Starting approximation',
                '2014-05-07 19:07:46,515 [main] TRACE eu.optique.api.component.omm.approximators.OntologyLocalApproximator - Done, saving',
                '2014-05-07 19:07:47,368 [main] INFO  eu.optique.api.component.omm.runners.ApproximatorRunner - All done, getting axiom statistics',
                '2014-05-07 19:07:49,023 [main] INFO  eu.optique.api.component.omm.runners.ApproximatorRunner - Original number of axioms: 36547',
                '2014-05-07 19:07:49,044 [main] INFO  eu.optique.api.component.omm.runners.ApproximatorRunner - Approximation number of axioms: 70272']):
      values.update(processLine(l))
    sql2('insert or replace into localresults values (?,?,?,?)',('EL-GALEN', values['Approximation number of axioms'], None, getMilis('total',values)))
    sql2('insert or replace into ontologies values (?,?)',('EL-GALEN', values['Original number of axioms']))
    
    