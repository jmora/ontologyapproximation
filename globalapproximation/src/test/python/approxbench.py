#!/usr/bin/env python3

import sys, os, os.path, re, sqlite3, datetime, subprocess, multiprocessing, traceback

tables = (['comparisonresults (ontology text PRIMARY KEY, entailedsize numeric)',
           'globalresults (ontology text PRIMARY KEY, appxsize numeric, entailedsize numeric, timereasoning numeric, totaltime numeric)',
           'localresults (ontology text PRIMARY KEY, appxsize numeric, entailedsize numeric, totaltime numeric)',
           'ontologies (ontology text PRIMARY KEY, originalsize numeric)',
           'problems (ontology text, problem text)'])

relevantTimes = {'reasoner': ('Precomputing inferences', 'Precomputing hierarchies'), 'total': ('Initializing approximation', 'All done, getting axiom statistics')}
localname = lambda x : x.split('/')[-1].split('\\')[-1].split('.')[0].replace('_', ' ').replace('ontology','')

def getMilis(code, values):
  d = values[relevantTimes[code][1]] - values[relevantTimes[code][0]]
  return d.seconds * 1000 + d.microseconds / 1000

def getTree(filename):
  return (os.path.join(root, name) for root, dirs, files in os.walk(filename, topdown=False) for name in files if name[-4:] == '.owl' and name[-7:-4] != 'apx')

def cleanTree(filename):
  for rem in (os.path.join(root, name) for root, dirs, files in os.walk(filename, topdown=False) for name in files if name[-4:] == '.log' or name[-7:-4] == 'apx'):
    try:
      os.remove(rem)
    except:
      pass
  
def processLog(log):
  values = {}
  for l in log:
    values.update(processLine(l))
  return values

def processLine(l):
  r = {} # sorry for next line, but that's the way I like it.
  m = re.match(r"(?P<year>\d+)-(?P<month>\d+)-(?P<day>\d+) (?P<hour>\d+):(?P<minute>\d+):(?P<second>\d+),(?P<microsecond>\d+)[\w \.\[\]]*- (?P<step>[\w ,\.\d:]+)", l)
  if m is None:
    return r
  m = m.groupdict()
  params = ['year', 'month', 'day', 'hour', 'minute', 'second', 'microsecond']
  d = {}
  for p in params:
    d[p] = int(m[p])
  d['microsecond'] = d['microsecond'] * 1000 # Yes, I lied.
  (s, a) = cleanStep(m['step'])
  r[s] = a
  if a is None:
    r[s] = datetime.datetime(**d)
  return r

def cleanStep(s):
  r = s.split(':')
  if len(r) == 1:
    return (s, None)
  if r[1].isdigit():
    return (r[0], int(r[1]))
  return (r[0], ':'.join(r[1:]))

def checkerrors(db, f, values):
  if ('Problem detected' in values):
    db.cursor().execute('insert into problems values (?,?)',(localname(f), values['Problem detected']))
    
def processOntology(f):
  res = {'problems':[], 'f': f}
  problems = res['problems']
  print('%s - testing ontology: %s (local first)'%(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),localname(f)))
  scope = 'local'
  start = 0
  r = None
  try:
    with open(f+'.local.log', 'w', encoding='utf8') as log:
      try:
        start = datetime.datetime.now()
        r = subprocess.call(['java', '-Xmx6144m', '-cp', 'approximator.jar', 'eu.optique.api.component.omm.runners.TerminalLASPX', f, f+'localapx.owl'], stdout=log, stderr=subprocess.STDOUT, timeout=28800)
      except:
        problems.append('Timeout (in local)')
    with open(f+".local.log", 'r') as log:
      res['local'] = processLog(log)
  except:
    problems.append('Execution terminated abruptly (in global) after %s'%str(start-datetime.datetime.now()))
  if r is None:
    problems.append('Timeout (in local)')
  print('%s - running global approximator for ontology: %s'%(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),localname(f)))
  scope = 'global'
  try:
    with open(f+'.global.log', 'w', encoding='utf8') as log:
      try:
        start = datetime.datetime.now()
        r = None
        r = subprocess.call(['java', '-Xmx6144m', '-cp', 'approximator.jar', 'eu.optique.api.component.omm.runners.TerminalGASPX', f, f+'globalapx.owl'], stdout=log, stderr=subprocess.STDOUT, timeout=28800)
      except:
        problems.append('Timeout (in global)')
    with open(f+".global.log", 'r') as log:
      res['global'] = processLog(log)
    if res['local']['Original number of axioms'] != res['global']['Original number of axioms']:
      problems.append("The original size of the ontology doesn't match for both approximations")
  except:
    problems.append('Execution terminated abruptly (in local) after %s'%str(start-datetime.datetime.now()))
  if r is None:
    problems.append('Timeout (in local)')
  print('%s - Comparing approximations for: %s'%(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),localname(f)))
  try:
    with open(f+'.compare.log', 'w', encoding='utf8') as log:
      try:
        start = datetime.datetime.now()
        r = None
        r = subprocess.call(['java', '-Xmx6144m', '-cp', 'approximator.jar', 'eu.optique.api.component.omm.runners.ApproximatorComparator', f+'globalapx.owl', f+'localapx.owl', f], stdout=log, stderr=subprocess.STDOUT, timeout=28800)
      except:
        problems.append('Timeout (in comparison)')
    with open(f+".compare.log", 'r') as log:
      res['compare'] = processLog(log)
  except:
    problems.append('Execution terminated abruptly (in comparison) after %s'%str(start-datetime.datetime.now()))
  return res

if __name__ == "__main__":
  if (len(sys.argv) > 1):
    testpath = sys.argv[1]
  else:
    testpath = 'finalExperiments'
  with sqlite3.connect('appxresults.sqlite3') as db:
    for t in tables:
      db.cursor().execute('create table if not exists ' + t)
    files = list(getTree(testpath))
    total = len(files)
    if total == 0:
      total = 1
    i = 0
    print('%s - <> Current progress: %.02f%%'%(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),100*i/total))
    with multiprocessing.Pool(processes=2) as pool:
      for res in pool.imap_unordered(processOntology, files):
        try:
          f = res['f']
          values = res['local']
          db.cursor().execute('insert or replace into localresults values (?,?,?,?)',(localname(f),
                      values['Approximation number of axioms'], values['Entailing original axioms'], getMilis('total',values)))
          db.cursor().execute('insert or replace into ontologies values (?,?)', (localname(f), values['Original number of axioms']))
          checkerrors(db, f, values)
          values = res['global']
          db.cursor().execute('insert or replace into globalresults values (?,?,?,?,?)',(localname(f),
                      values['Approximation number of axioms'], values['Entailing original axioms'],
                      getMilis('reasoner', values), getMilis('total',values)))
          checkerrors(db, f, values)
          values = res['compare']
          db.cursor().execute('insert or replace into comparisonresults values (?,?)', (localname(f), values['Entailment of global approximation by local in axioms']))
          checkerrors(db, f, values)
          for problem in res['problems']:
            checkerrors(db, f, {'Problem detected': problem})
        except Exception as e:
          print('%s - !! Critical error for: %s'%(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),localname(f)))
          checkerrors(db, f, {'Problem detected': 'missing values'})
        i += 1
        print('%s - <> Current progress: %.02f%%'%(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),100*i/total))
        db.commit()
