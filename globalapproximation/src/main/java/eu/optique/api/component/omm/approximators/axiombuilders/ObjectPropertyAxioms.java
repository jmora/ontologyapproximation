package eu.optique.api.component.omm.approximators.axiombuilders;

import java.util.Collection;
import java.util.HashSet;

import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import eu.optique.api.component.omm.approximators.preprocess.HierarchyBuilder;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public class ObjectPropertyAxioms {

	private HierarchyBuilder hierarchyBuilder;
	private AxiomFactory axiomFactory;
	private OWLReasoner reason;

	private ObjectPropertyAxioms(HierarchyBuilder hierarchyBuilder, AxiomFactory axiomFactory, OWLReasoner reason) {
		this.hierarchyBuilder = hierarchyBuilder;
		this.axiomFactory = axiomFactory;
		this.reason = reason;
	}

	public static Collection<OWLAxiom> compute (HierarchyBuilder hierarchyBuilder, AxiomFactory axiomFactory, OWLReasoner reason) {
		ObjectPropertyAxioms d = new ObjectPropertyAxioms(hierarchyBuilder, axiomFactory, reason);
		return d.recComputation(axiomFactory.getOWLDataFactory().getOWLTopObjectProperty());
	}

	private Collection<OWLAxiom> recComputation (OWLObjectPropertyExpression P) {
		Collection<OWLAxiom> ret = new HashSet<OWLAxiom>();
		if (!P.isTopEntity() && !P.isBottomEntity()) {
			OWLAxiom sym = this.axiomFactory.getOWLDataFactory().getOWLSymmetricObjectPropertyAxiom(P);
			OWLAxiom asym = this.axiomFactory.getOWLDataFactory().getOWLAsymmetricObjectPropertyAxiom(P);
			OWLAxiom ref = this.axiomFactory.getOWLDataFactory().getOWLReflexiveObjectPropertyAxiom(P);
			OWLAxiom irref = this.axiomFactory.getOWLDataFactory().getOWLIrreflexiveObjectPropertyAxiom(P);
			if (this.reason.isEntailed(sym))
				ret.add(sym);
			if (this.reason.isEntailed(asym))
				ret.add(asym);
			if (this.reason.isEntailed(ref))
				ret.add(ref);
			if (this.reason.isEntailed(irref))
				ret.add(irref);
		}
		Collection<OWLObjectPropertyExpression> Pcs = this.hierarchyBuilder.getChildrenOf(P, true);
		for (OWLObjectPropertyExpression Pc : Pcs)
			ret.addAll(this.recComputation(Pc));
		return ret;
	}
}