package eu.optique.api.component.omm.approximators.runners;

import java.io.IOException;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

import eu.optique.api.component.omm.approximators.OntologyApproximator;
import eu.optique.api.component.omm.approximators.OntologyGlobalApproximator;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public class TerminalGASPX {

	public static void main (String[] args) throws OWLOntologyCreationException, OWLOntologyStorageException, IOException {
		OntologyApproximator approximator = new OntologyGlobalApproximator();
		new ApproximatorRunner(approximator).run(args);
	}

}
