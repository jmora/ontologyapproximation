package eu.optique.api.component.omm.approximators;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.profiles.OWL2QLProfile;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public class OntologyLocalApproximator extends AbstractOntologyApproximator {

	static private Logger logger = Logger.getLogger(OntologyLocalApproximator.class.getName());

	public OWLOntology approximate (OWLOntology sourceOntology, OWLReasonerFactory reasonerFactory) throws OWLOntologyCreationException {
		Set<OWLAxiom> approximatedAxioms = new HashSet<OWLAxiom>();
		OntologyGlobalApproximator globalApproximator = new OntologyGlobalApproximator();
		if (new OWL2QLProfile().checkOntology(sourceOntology).isInProfile()) {
			OntologyLocalApproximator.logger.trace("Done, saving");
			OntologyLocalApproximator.logger.trace("Problem detected: ontology already in the OWL2 QL profile");
			return sourceOntology;
		}
		OntologyLocalApproximator.logger.trace("Starting approximation");
		for (OWLAxiom axiom : sourceOntology.getAxioms()) {
			HashSet<OWLAxiom> currentAxioms = new HashSet<OWLAxiom>();
			currentAxioms.add(axiom);
			approximatedAxioms.addAll(globalApproximator.approximate(currentAxioms, reasonerFactory));
		}

		OWLOntology targetOntology = this.createOntologyFromAxiomSet(approximatedAxioms, sourceOntology);
		OntologyLocalApproximator.logger.trace("Done, saving");
		return targetOntology;
	}

}
