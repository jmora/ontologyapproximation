package eu.optique.api.component.omm.approximators.runners;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public class UnifiedCLI {
	static List<String> options = Arrays.asList("-local", "-global", "-compare");

	public static void main (String[] args) throws IOException, OWLOntologyCreationException, OWLOntologyStorageException {
		if (args.length < 2 || !UnifiedCLI.options.contains(args[0])) {
			System.out.println("Usage:  java -jar approximator.jar mode ontologyfile\n\twhere mode is one among: -local -global and -compare\n"
					+ "Approximated ontologies will not be overwritten if they are found.");
			return;
		}
		File local = UnifiedCLI.getOutputFile(args[1], "local-");
		File global = UnifiedCLI.getOutputFile(args[1], "global-");
		if (args[0].equals("-local") || args[0].equals("-compare") && !local.exists())
			TerminalLASPX.main(UnifiedCLI.makeArgs(args[1], local));
		if (args[0].equals("-global") || args[0].equals("-compare") && !global.exists())
			TerminalGASPX.main(UnifiedCLI.makeArgs(args[1], global));
		if (args[0].equals("-compare")) {
			String[] nargs = { global.toPath().toString(), local.toPath().toString(), args[1] };
			ApproximatorComparator.main(nargs);
		}
	}

	private static File getOutputFile (String original, String prefix) throws IOException {
		File originalFile = new File(original);
		return new File(originalFile.getParentFile().getPath() + File.separator + prefix + originalFile.getName());
	}

	private static String[] makeArgs (String original, File out) {
		String[] s = { original, out.toPath().toString() };
		return s;
	}
}
