package eu.optique.api.component.omm.approximators.runners;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.apache.log4j.Logger;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.HermiT.Reasoner.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.profiles.OWL2QLProfile;
import org.semanticweb.owlapi.profiles.OWLProfileReport;
import org.semanticweb.owlapi.profiles.OWLProfileViolation;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import eu.optique.api.component.omm.approximators.OntologyApproximator;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public class ApproximatorRunner {

	static private ReasonerFactory reasonerFactory = new Reasoner.ReasonerFactory();

	private OntologyApproximator approximator;

	private Logger logger;

	public ApproximatorRunner(OntologyApproximator approximator) {
		this.approximator = approximator;
		this.logger = Logger.getLogger(approximator.getClass().getName());
	}

	public void run (String[] args) throws OWLOntologyCreationException, OWLOntologyStorageException, IOException {
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		File outputFile = new File(args[1]);
		if (!outputFile.exists())
			outputFile.createNewFile();
		this.logger.info("Initializing approximation");
		OWLOntology sourceOntology = manager.loadOntologyFromOntologyDocument(new File(args[0]));
		OWLOntology targetOntology = this.approximator.approximate(sourceOntology, ApproximatorRunner.reasonerFactory);
		manager.setOntologyFormat(targetOntology, manager.getOntologyFormat(sourceOntology));
		manager.saveOntology(targetOntology, IRI.create(outputFile.toURI()));
		this.logger.info("All done, getting axiom statistics");
		this.getStatistics(args);
		this.testValidApproximation(targetOntology, sourceOntology);
	}

	private void getStatistics (String[] args) throws OWLOntologyCreationException {
		OWLOntology sourceOntology = OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(new File(args[0]));
		OWLOntology targetOntology = OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(new File(args[1]));
		this.logger.info("Original number of axioms: " + sourceOntology.getLogicalAxiomCount());
		this.logger.info("Approximation number of axioms: " + targetOntology.getLogicalAxiomCount());
		OWLReasoner reasoner = ApproximatorRunner.reasonerFactory.createReasoner(targetOntology);
		reasoner.precomputeInferences();
		int entailed = 0;
		for (OWLAxiom axiom : sourceOntology.getLogicalAxioms())
			if (reasoner.isEntailed(axiom))
				entailed++;
		this.logger.info("Entailing original axioms: " + entailed);
	}

	public boolean testValidApproximation (OWLOntology targetOntology, OWLOntology sourceOntology) {
		OWLReasoner reasoner = ApproximatorRunner.reasonerFactory.createReasoner(sourceOntology);
		boolean result = true;
		OWLProfileReport report = new OWL2QLProfile().checkOntology(targetOntology);
		if (!report.isInProfile()) {
			this.logger.debug("Problem detected: Approximation beyond OWL2 QL");
			for (OWLProfileViolation violation : report.getViolations())
				this.logger.debug("Problem detected: Beyond OWL2 QL in " + violation.getAxiom());
			result = false;
		}
		reasoner.precomputeInferences();
		for (OWLAxiom axiom : targetOntology.getLogicalAxioms())
			if (!reasoner.isEntailed(axiom)) {
				this.logger.debug("Problem detected: Not entailed axiom found, it is " + axiom.toString());
				result = false;
			}
		Set<OWLEntity> sourceSignature = sourceOntology.getSignature();
		for (OWLEntity entity : targetOntology.getSignature())
			if (!sourceSignature.contains(entity)) {
				this.logger.debug("Problem detected: signature changed, extra symbol is " + entity.toString());
				result = false;
			}
		return result;
	}

}
