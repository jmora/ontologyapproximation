package eu.optique.api.component.omm.approximators;

import java.util.ArrayList;
import java.util.Collection;

import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;

import eu.optique.api.component.omm.approximators.axiombuilders.AxiomFactory;
import eu.optique.api.component.omm.approximators.preprocess.HierarchyBuilder;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public class ExistentialExplorer {

	private AxiomFactory axiomFactory;
	private HierarchyBuilder hierarchies;
	private OWLDataFactory factory;
	private ArrayList<OWLObjectPropertyExpression> propertiesAlreadyChecked;
	private ArrayList<OWLClassExpression> existentialsAlreadyChecked;

	public ExistentialExplorer(HierarchyBuilder hierarchies, AxiomFactory axiomFactory) {
		this.hierarchies = hierarchies;
		this.axiomFactory = axiomFactory;
		this.factory = axiomFactory.getOWLDataFactory();
		this.propertiesAlreadyChecked = new ArrayList<OWLObjectPropertyExpression>();
		this.existentialsAlreadyChecked = new ArrayList<OWLClassExpression>();
	}

	public Collection<OWLAxiom> getAxioms () {
		ArrayList<OWLAxiom> res = new ArrayList<OWLAxiom>();
		for (OWLObjectPropertyExpression role : this.hierarchies.getChildrenOf(this.factory.getOWLTopObjectProperty(), true))
			res.addAll(this.getAxiomsForRoleTree(role));
		return res;
	}

	private Collection<OWLAxiom> getAxiomsForRoleTree (OWLObjectPropertyExpression role) {
		ArrayList<OWLAxiom> res = new ArrayList<OWLAxiom>();
		if (this.propertiesAlreadyChecked.contains(role))
			return res;
		this.propertiesAlreadyChecked.add(role);
		for (OWLClass concept : this.hierarchies.getChildrenOf(this.factory.getOWLThing(), true)) {
			Collection<? extends OWLAxiom> partialRes = this.getAxiomsForConceptTree(role, concept);
			if (!partialRes.isEmpty()) {
				for (OWLObjectPropertyExpression subrole : this.hierarchies.getChildrenOf(role, true))
					res.addAll(this.getAxiomsForRoleTree(subrole));
				res.addAll(partialRes);
			}
		}
		return res;
	}

	private Collection<OWLAxiom> getAxiomsForConceptTree (OWLObjectPropertyExpression role, OWLClass concept) {
		Collection<OWLAxiom> res = new ArrayList<OWLAxiom>();
		OWLClassExpression expression = this.factory.getOWLObjectSomeValuesFrom(role, concept);
		if (this.existentialsAlreadyChecked.contains(expression) || concept.isBottomEntity())
			return res;
		this.existentialsAlreadyChecked.add(expression);
		res = this.getAxiomsForExpression(expression);
		if (!res.isEmpty()) {
			if (this.axiomFactory.getNameManager().isArtificial(concept))
				res = new ArrayList<OWLAxiom>();
			for (OWLClass subconcept : this.hierarchies.getChildrenOf(concept, true))
				res.addAll(this.getAxiomsForConceptTree(role, subconcept));
		}
		return res;
	}

	private Collection<OWLAxiom> getAxiomsForExpression (OWLClassExpression expression) {
		// TODO: getting only direct children would produce (maybe more efficiently) a more compact result, equally complete
		return this.axiomFactory.subAxioms(expression, this.hierarchies.getChildrenOf(expression, false));
	}
}
