package eu.optique.api.component.omm.approximators.preprocess;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLDataSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.vocab.OWL2Datatype;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public class NameManager {

	private OWLOntology ontology;
	private OWLOntologyManager ontologyManager;
	private OWLDataFactory factory;
	private String baseIRI = "http://www.optique-project.eu/‎gsapx/ontology/concept/auxiliary";
	private int auxiliaryConcepts = 0;
	private HashMap<IRI, OWLClassExpression> correspondences;
	private ArrayList<OWLDatatype> approximationDatatypes;
	private HashMap<OWLClassExpression, OWLClass> inversecorrespondences;

	public NameManager(OWLOntology ontology) throws OWLOntologyCreationException {
		this.initialize(ontology.getAxioms());
	}

	public NameManager(Set<OWLAxiom> axioms) throws OWLOntologyCreationException {
		this.initialize(axioms);
	}

	private void initialize (Set<OWLAxiom> axioms) throws OWLOntologyCreationException {
		this.ontologyManager = OWLManager.createOWLOntologyManager();
		this.factory = this.ontologyManager.getOWLDataFactory();
		this.ontology = this.ontologyManager.createOntology(axioms);
		this.correspondences = new HashMap<IRI, OWLClassExpression>();
		this.approximationDatatypes = new ArrayList<OWLDatatype>();
		this.inversecorrespondences = new HashMap<OWLClassExpression, OWLClass>();
		this.fillDatatypes();
		this.introduceNames();

	}

	private void fillDatatypes () {
		// this.approximationDatatypes.addAll(this.ontology.getDatatypesInSignature());
		this.approximationDatatypes.addAll(this.allOWL2QLDatatypes());
	}

	public Collection<OWLDatatype> allOWL2QLDatatypes () {
		// as of http://www.w3.org/TR/owl2-profiles/#Entities_2
		ArrayList<OWLDatatype> result = new ArrayList<OWLDatatype>();
		for (OWL2Datatype dataType : Arrays.asList(OWL2Datatype.RDF_PLAIN_LITERAL, OWL2Datatype.RDF_XML_LITERAL, OWL2Datatype.RDFS_LITERAL, OWL2Datatype.OWL_REAL, OWL2Datatype.OWL_RATIONAL,
				OWL2Datatype.XSD_DECIMAL, OWL2Datatype.XSD_INTEGER, OWL2Datatype.XSD_NON_NEGATIVE_INTEGER, OWL2Datatype.XSD_STRING, OWL2Datatype.XSD_NORMALIZED_STRING, OWL2Datatype.XSD_TOKEN,
				OWL2Datatype.XSD_NAME, OWL2Datatype.XSD_NCNAME, OWL2Datatype.XSD_NMTOKEN, OWL2Datatype.XSD_HEX_BINARY, OWL2Datatype.XSD_BASE_64_BINARY, OWL2Datatype.XSD_ANY_URI,
				OWL2Datatype.XSD_DATE_TIME, OWL2Datatype.XSD_DATE_TIME_STAMP))
			result.add(this.factory.getOWLDatatype(dataType.getIRI()));
		return result;
	}

	private void introduceNames () {
		for (OWLObjectProperty objectProperty : this.ontology.getObjectPropertiesInSignature())
			if (!objectProperty.isTopEntity()) {
				this.introduceName(this.makeExistentialClass(objectProperty));
				this.introduceName(this.makeExistentialClass(this.factory.getOWLObjectInverseOf(objectProperty)));
			}
		for (OWLDataProperty dataProperty : this.ontology.getDataPropertiesInSignature())
			if (!dataProperty.isTopEntity())
				for (OWLDatatype dataType : this.approximationDatatypes)
					this.introduceName(this.makeExistentialClass(dataProperty, dataType));
	}

	private OWLObjectSomeValuesFrom makeExistentialClass (OWLObjectPropertyExpression objectProperty) {
		return this.factory.getOWLObjectSomeValuesFrom(objectProperty, this.factory.getOWLThing());
	}

	private OWLDataSomeValuesFrom makeExistentialClass (OWLDataPropertyExpression dataProperty, OWLDatatype owlDatatype) {
		return this.factory.getOWLDataSomeValuesFrom(dataProperty, owlDatatype);
	}

	private void introduceName (OWLClassExpression classExpression) {
		IRI newUniqueIRI = IRI.create(this.baseIRI + this.auxiliaryConcepts++);
		this.correspondences.put(newUniqueIRI, classExpression);
		OWLClass owlClass = this.factory.getOWLClass(newUniqueIRI);
		this.inversecorrespondences.put(classExpression, owlClass);
		OWLEquivalentClassesAxiom axiom = this.factory.getOWLEquivalentClassesAxiom(owlClass, classExpression);
		this.ontologyManager.addAxiom(this.ontology, axiom);
	}

	public OWLClassExpression getExpressionFromName (OWLClass owlClass) {
		if (owlClass.isAnonymous() || !this.correspondences.containsKey(owlClass.getIRI()))
			return owlClass;
		return this.correspondences.get(owlClass.getIRI());
	}

	public OWLOntology getNamedOntology () {
		return this.ontology;
	}

	public boolean isArtificial (OWLClass owlClass) {
		return !owlClass.isAnonymous() && this.correspondences.containsKey(owlClass.getIRI());
	}

	public OWLClass getNamedEquivalent (OWLClassExpression classExpression) {
		return this.inversecorrespondences.get(classExpression);
	}

}
