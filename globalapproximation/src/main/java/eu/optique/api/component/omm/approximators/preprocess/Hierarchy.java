package eu.optique.api.component.omm.approximators.preprocess;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Marco Console and Jose Mora
 * 
 * @param <T>
 */
public class Hierarchy<T> {

	private HashMap<T, Collection<T>> hierarchy = new HashMap<T, Collection<T>>();

	public Collection<T> put (T key, Collection<T> value) {
		return this.hierarchy.put(key, value);
	}

	public Collection<T> remove (T key) {
		return this.hierarchy.remove(key);
	}

	public Collection<T> get (T key) {
		return this.hierarchy.get(key);
	}

	public Map<T, Collection<T>> asMap () {
		return this.hierarchy;
	}

	public boolean containsKey (T key) {
		return this.hierarchy.containsKey(key);
	}

	public Collection<T> get (T key, boolean direct) {
		if (direct)
			return this.hierarchy.get(key);
		return this.getRecursiveChildren(key);
	}

	private Collection<T> getRecursiveChildren (T item) {
		ArrayList<T> res = new ArrayList<T>();
		Collection<T> children = this.hierarchy.get(item);
		res.addAll(children);
		for (T child : children)
			res.addAll(this.getRecursiveChildren(child));
		return res;
	}

}
