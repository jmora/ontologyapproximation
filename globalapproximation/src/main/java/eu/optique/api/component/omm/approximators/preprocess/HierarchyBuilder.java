package eu.optique.api.component.omm.approximators.preprocess;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public class HierarchyBuilder {
	private OWLOntologyManager ontologyManager;
	private OWLDataFactory factory;
	private OWLReasoner reasoner;
	private Hierarchy<OWLClass> conceptHierarchy;
	private Hierarchy<OWLObjectPropertyExpression> rolesHierarchy;
	private Hierarchy<OWLDataProperty> attributeHierarchy;
	private boolean directChildren = true;

	public HierarchyBuilder() {
	}

	public HierarchyBuilder(OWLOntology ontology, OWLReasoner reasoner) {
		this.initialize(ontology, reasoner);
	}

	public void initialize (OWLOntology ontology, OWLReasoner reasoner) {
		this.reasoner = reasoner;
		this.ontologyManager = OWLManager.createOWLOntologyManager();
		this.factory = this.ontologyManager.getOWLDataFactory();
		this.reasoner = reasoner;
		this.precomputeConceptHierarchy();
		this.precomputeRoleHierarchy();
		this.precomputeAttributeHierarchy();
	}

	private void precomputeConceptHierarchy () {
		this.conceptHierarchy = this.addSubclassesRec(new Hierarchy<OWLClass>(), this.factory.getOWLThing());
	}

	private Hierarchy<OWLClass> addSubclassesRec (Hierarchy<OWLClass> res, OWLClass parent) {
		Set<OWLClass> children = this.reasoner.getSubClasses(parent, this.directChildren).getFlattened();
		res.put(parent, children);
		for (OWLClass child : children)
			res = this.addSubclassesRec(res, child);
		return res;
	}

	private void precomputeRoleHierarchy () {
		OWLObjectPropertyExpression topObjectProperty = this.reasoner.getTopObjectPropertyNode().getRepresentativeElement();
		this.rolesHierarchy = this.addRolesRec(new Hierarchy<OWLObjectPropertyExpression>(), topObjectProperty);
	}

	private Hierarchy<OWLObjectPropertyExpression> addRolesRec (Hierarchy<OWLObjectPropertyExpression> hierarchy, OWLObjectPropertyExpression parent) {
		Set<OWLObjectPropertyExpression> children = this.reasoner.getSubObjectProperties(parent, this.directChildren).getFlattened();
		hierarchy.put(parent, children);
		for (OWLObjectPropertyExpression child : children)
			hierarchy = this.addRolesRec(hierarchy, child);
		return hierarchy;
	}

	private void precomputeAttributeHierarchy () {
		this.attributeHierarchy = this.addAttributesRec(new Hierarchy<OWLDataProperty>(), this.factory.getOWLTopDataProperty());
	}

	private Hierarchy<OWLDataProperty> addAttributesRec (Hierarchy<OWLDataProperty> hierarchy, OWLDataProperty parent) {
		Set<OWLDataProperty> children = this.reasoner.getSubDataProperties(parent, this.directChildren).getFlattened();
		hierarchy.put(parent, children);
		for (OWLDataProperty child : children)
			hierarchy = this.addAttributesRec(hierarchy, child);
		return hierarchy;
	}

	public Collection<OWLClass> getChildrenOf (OWLClassExpression owlClassExpression, boolean direct) {
		if (owlClassExpression.isAnonymous() || !this.conceptHierarchy.containsKey(owlClassExpression.asOWLClass()))
			return this.reasoner.getSubClasses(owlClassExpression, direct).getFlattened();
		return this.conceptHierarchy.get(owlClassExpression.asOWLClass(), direct);
	}

	public Collection<OWLObjectPropertyExpression> getChildrenOf (OWLObjectPropertyExpression owlObjectPropertyExpression, boolean direct) {
		if (!this.rolesHierarchy.containsKey(owlObjectPropertyExpression))
			return this.reasoner.getSubObjectProperties(owlObjectPropertyExpression, direct).getFlattened();
		return this.rolesHierarchy.get(owlObjectPropertyExpression, direct);
	}

	public Collection<OWLDataProperty> getChildrenOf (OWLDataProperty owlDataProperty, boolean direct) {
		if (!this.attributeHierarchy.containsKey(owlDataProperty))
			return this.reasoner.getSubDataProperties(owlDataProperty, direct).getFlattened();
		return this.attributeHierarchy.get(owlDataProperty, direct);
	}

	public Collection<OWLClass> getDisjointClasses (OWLClassExpression owlClassExpression) {
		Set<OWLClass> Ds = this.reasoner.getDisjointClasses(owlClassExpression).getFlattened();
		Ds.remove(this.factory.getOWLNothing());
		if (Ds.contains(this.factory.getOWLThing())) {
			Ds = new HashSet<OWLClass>();
			Ds.add(this.factory.getOWLThing());
		}
		return Ds;
	}

	public Collection<OWLObjectPropertyExpression> getDisjointObjectProperties (OWLObjectPropertyExpression owlObjectPropertyExpression) {
		Set<OWLObjectPropertyExpression> Ds = this.reasoner.getDisjointObjectProperties(owlObjectPropertyExpression).getFlattened();
		Ds.remove(this.factory.getOWLBottomObjectProperty());
		if (Ds.contains(this.factory.getOWLTopObjectProperty())) {
			Ds = new HashSet<OWLObjectPropertyExpression>();
			Ds.add(this.factory.getOWLTopObjectProperty());
		}
		return Ds;
	}

	public Collection<OWLDataProperty> getDisjointDataProperties (OWLDataPropertyExpression owlDataPropertyExpression) {
		Set<OWLDataProperty> Ds = this.reasoner.getDisjointDataProperties(owlDataPropertyExpression).getFlattened();
		Ds.remove(this.factory.getOWLBottomDataProperty());
		if (Ds.contains(this.factory.getOWLTopDataProperty())) {
			Ds = new HashSet<OWLDataProperty>();
			Ds.add(this.factory.getOWLTopDataProperty());
		}
		return Ds;
	}

	public Collection<OWLClass> getEquivalentClasses (OWLClassExpression owlClassExpression) {
		return this.reasoner.getEquivalentClasses(owlClassExpression).getEntities();
	}

	public Collection<OWLObjectPropertyExpression> getEquivalentObjectProperties (OWLObjectPropertyExpression owlObjectPropertyExpression) {
		return this.reasoner.getEquivalentObjectProperties(owlObjectPropertyExpression).getEntities();
	}

	public Collection<OWLDataProperty> getEquivalentDataProperties (OWLDataProperty owlDataPropertyExpression) {
		return this.reasoner.getEquivalentDataProperties(owlDataPropertyExpression).getEntities();
	}

}
