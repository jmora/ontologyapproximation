package eu.optique.api.component.omm.approximators.axiombuilders;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;

import eu.optique.api.component.omm.approximators.preprocess.HierarchyBuilder;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public class ExistentialsChildren {

	private HierarchyBuilder b;
	private HashSet<OWLAxiom> ret;
	private OWLDataFactory factory;
	private Collection<OWLClassExpression> done;
	private AxiomFactory axiomFactory;

	private ExistentialsChildren(HierarchyBuilder b, AxiomFactory axiomFactory) {
		this.b = b;
		this.ret = new HashSet<OWLAxiom>();
		this.factory = axiomFactory.getOWLDataFactory();
		this.done = new HashSet<OWLClassExpression>();
		this.axiomFactory = axiomFactory;
	}

	public static Collection<OWLAxiom> compute (HierarchyBuilder b, AxiomFactory axiomFactory) {

		ExistentialsChildren ex = new ExistentialsChildren(b, axiomFactory);

		ex.recComputation(ex.factory.getOWLThing(), ex.factory.getOWLTopObjectProperty());
		return ex.ret;
	}

	private void recComputation (OWLClass C, OWLObjectPropertyExpression P) {

		if (P.isTopEntity()) {
			Collection<OWLObjectPropertyExpression> Pcs = this.b.getChildrenOf(P, true);
			for (OWLObjectPropertyExpression Pc : Pcs)
				this.recComputation(C, Pc);
		} else {
			OWLClassExpression ex = this.factory.getOWLObjectSomeValuesFrom(P, C);

			Set<OWLClass> Ccs = new HashSet<OWLClass>();

			if (!this.done.contains(ex))
				Ccs.addAll(this.b.getChildrenOf(ex, false));

			Ccs.remove(this.factory.getOWLNothing());

			this.done.add(ex);

			if (!this.axiomFactory.isArtificial(C))
				this.ret.addAll(this.axiomFactory.subAxioms(ex, Ccs));

			if (!Ccs.isEmpty()) {
				Collection<OWLObjectPropertyExpression> Pcs = this.b.getChildrenOf(P, true);
				for (OWLObjectPropertyExpression Pc : Pcs)
					this.recComputation(C, Pc);
				Collection<OWLClass> Cs = this.b.getChildrenOf(C, true);
				for (OWLClass Cc : Cs)
					this.recComputation(Cc, P);
			}

		}
	}
}
