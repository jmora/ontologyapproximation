package eu.optique.api.component.omm.approximators.axiombuilders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;

import eu.optique.api.component.omm.approximators.preprocess.HierarchyBuilder;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public class Disjointnesses {
	private AxiomFactory axiomFactory;
	private HierarchyBuilder hierarchyBuilder;
	private ArrayList<OWLClass> alreadyTestedClasses;
	private ArrayList<OWLObjectPropertyExpression> alreadyTestedRoles;
	private ArrayList<OWLDataProperty> alreadyTestedAttributes;
	private ArrayList<OWLClass> exploredClasses;
	private ArrayList<OWLObjectPropertyExpression> exploredRoles;
	private ArrayList<OWLDataProperty> exploredAttributes;
	private OWLDataFactory factory;

	public Disjointnesses(HierarchyBuilder hierarchyBuilder, AxiomFactory axiomFactory) {
		this.hierarchyBuilder = hierarchyBuilder;
		this.axiomFactory = axiomFactory;
		this.factory = axiomFactory.getOWLDataFactory();
		this.alreadyTestedClasses = new ArrayList<OWLClass>();
		this.alreadyTestedClasses.add(this.factory.getOWLNothing());
		this.alreadyTestedRoles = new ArrayList<OWLObjectPropertyExpression>();
		this.alreadyTestedAttributes = new ArrayList<OWLDataProperty>();
		this.alreadyTestedRoles.add(this.factory.getOWLBottomObjectProperty());
		this.alreadyTestedAttributes.add(this.factory.getOWLBottomDataProperty());
		this.exploredClasses = new ArrayList<OWLClass>(this.alreadyTestedClasses);
		this.exploredRoles = new ArrayList<OWLObjectPropertyExpression>(this.alreadyTestedRoles);
		this.exploredAttributes = new ArrayList<OWLDataProperty>(this.alreadyTestedAttributes);
	}

	public Collection<OWLAxiom> compute () {
		Collection<OWLAxiom> ret = new HashSet<OWLAxiom>();
		Disjointnesses d = new Disjointnesses(this.hierarchyBuilder, this.axiomFactory);
		Collection<OWLClass> CLs = d.getLeaves(this.axiomFactory.getOWLDataFactory().getOWLThing());
		for (OWLClass CL : CLs) {
			if (this.alreadyTestedClasses.contains(CL))
				continue;
			this.alreadyTestedClasses.add(CL);
			Collection<OWLClass> Ds = this.hierarchyBuilder.getDisjointClasses(CL);
			for (OWLClass D : Ds) {
				if (this.alreadyTestedClasses.contains(D))
					continue;
				this.alreadyTestedClasses.add(D);
				ret.addAll(this.axiomFactory.buildDisjointnessAxioms(D, this.hierarchyBuilder.getDisjointClasses(D)));
			}
		}

		Collection<OWLObjectPropertyExpression> PLs = d.getLeaves(this.axiomFactory.getOWLDataFactory().getOWLTopObjectProperty());
		for (OWLObjectPropertyExpression PL : PLs) {
			if (this.alreadyTestedRoles.contains(PL))
				continue;
			this.alreadyTestedRoles.add(PL);
			Collection<OWLObjectPropertyExpression> Ds = this.hierarchyBuilder.getDisjointObjectProperties(PL);
			for (OWLObjectPropertyExpression D : Ds) {
				if (this.alreadyTestedRoles.contains(D))
					continue;
				this.alreadyTestedRoles.add(D);
				ret.addAll(this.axiomFactory.buildDisjointnessAxioms(D, this.hierarchyBuilder.getDisjointObjectProperties(D)));
			}
		}

		Collection<OWLDataProperty> ULs = d.getLeaves(this.axiomFactory.getOWLDataFactory().getOWLTopDataProperty());
		for (OWLDataProperty UL : ULs) {
			if (this.alreadyTestedAttributes.contains(UL))
				continue;
			this.alreadyTestedAttributes.add(UL);
			Collection<OWLDataProperty> Ds = this.hierarchyBuilder.getDisjointDataProperties(UL);
			for (OWLDataProperty D : Ds) {
				if (this.alreadyTestedAttributes.contains(D))
					continue;
				this.alreadyTestedAttributes.add(D);
				ret.addAll(this.axiomFactory.buildDisjointnessAxioms(D, this.hierarchyBuilder.getDisjointDataProperties(D)));
			}
		}

		return ret;
	}

	private Collection<OWLClass> getLeaves (OWLClass C) {
		HashSet<OWLClass> ret = new HashSet<OWLClass>();
		Collection<OWLClass> Cs = this.hierarchyBuilder.getChildrenOf(C, true);
		Cs.remove(this.axiomFactory.getOWLDataFactory().getOWLNothing());
		if (Cs.isEmpty())
			ret.add(C);
		else
			for (OWLClass Cc : Cs)
				if (!this.exploredClasses.contains(Cc)) {
					this.exploredClasses.add(Cc);
					ret.addAll(this.getLeaves(Cc));
				}
		return ret;
	}

	private Collection<OWLObjectPropertyExpression> getLeaves (OWLObjectPropertyExpression P) {
		HashSet<OWLObjectPropertyExpression> ret = new HashSet<OWLObjectPropertyExpression>();
		Collection<OWLObjectPropertyExpression> Ps = this.hierarchyBuilder.getChildrenOf(P, true);
		Ps.remove(this.axiomFactory.getOWLDataFactory().getOWLBottomObjectProperty());
		if (Ps.isEmpty())
			ret.add(P);
		else
			for (OWLObjectPropertyExpression Cc : Ps)
				if (!this.exploredRoles.contains(Cc)) {
					this.exploredRoles.add(Cc);
					ret.addAll(this.getLeaves(Cc));
				}
		return ret;
	}

	private Collection<OWLDataProperty> getLeaves (OWLDataProperty U) {
		HashSet<OWLDataProperty> ret = new HashSet<OWLDataProperty>();
		Collection<OWLDataProperty> Ps = this.hierarchyBuilder.getChildrenOf(U, true);
		Ps.remove(this.axiomFactory.getOWLDataFactory().getOWLBottomObjectProperty());
		if (Ps.isEmpty())
			ret.add(U);
		else
			for (OWLDataProperty Cc : Ps)
				if (!this.exploredAttributes.contains(Cc)) {
					this.exploredAttributes.add(Cc);
					ret.addAll(this.getLeaves(Cc));
				}
		return ret;
	}
}
