package eu.optique.api.component.omm.approximators;

import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public abstract class AbstractOntologyApproximator implements OntologyApproximator {

	protected OWLOntology createOntologyFromAxiomSet (Set<OWLAxiom> axioms, OWLOntology sourceOntology) throws OWLOntologyCreationException {
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		IRI iri = sourceOntology.getOntologyID().getOntologyIRI();
		if (iri == null)
			iri = sourceOntology.getOntologyID().getDefaultDocumentIRI();
		OWLDataFactory factory = manager.getOWLDataFactory();
		for (OWLClass owlClass : sourceOntology.getClassesInSignature())
			axioms.add(factory.getOWLDeclarationAxiom(owlClass));
		for (OWLObjectProperty objectProperty : sourceOntology.getObjectPropertiesInSignature())
			axioms.add(factory.getOWLDeclarationAxiom(objectProperty));
		for (OWLDataProperty dataProperty : sourceOntology.getDataPropertiesInSignature())
			axioms.add(factory.getOWLDeclarationAxiom(dataProperty));
		OWLOntology targetOntology = manager.createOntology(axioms, iri);
		manager.setOntologyFormat(targetOntology, manager.getOntologyFormat(sourceOntology));
		return targetOntology;
	}

}
