package eu.optique.api.component.omm.approximators.axiombuilders;

import java.util.Collection;
import java.util.HashSet;

import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.vocab.OWL2Datatype;

import eu.optique.api.component.omm.approximators.preprocess.HierarchyBuilder;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public class DataPropertyRangeAxioms {

	private HierarchyBuilder hierarchyBuilder;
	private AxiomFactory axiomFactory;
	private OWLReasoner reason;

	private DataPropertyRangeAxioms(HierarchyBuilder hierarchyBuilder, AxiomFactory axiomFactory, OWLReasoner reason) {
		this.hierarchyBuilder = hierarchyBuilder;
		this.axiomFactory = axiomFactory;
		this.reason = reason;
	}

	public static Collection<OWLAxiom> compute (HierarchyBuilder hierarchyBuilder, AxiomFactory axiomFactory, OWLReasoner reason) {
		DataPropertyRangeAxioms d = new DataPropertyRangeAxioms(hierarchyBuilder, axiomFactory, reason);
		Collection<OWLDatatype> Dts = new HashSet<OWLDatatype>();

		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.RDF_PLAIN_LITERAL.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.RDF_XML_LITERAL.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.RDFS_LITERAL.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.OWL_REAL.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.OWL_RATIONAL.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.XSD_DECIMAL.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.XSD_INTEGER.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.XSD_NON_NEGATIVE_INTEGER.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.XSD_STRING.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.XSD_NORMALIZED_STRING.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.XSD_TOKEN.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.XSD_NAME.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.XSD_NCNAME.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.XSD_NMTOKEN.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.XSD_HEX_BINARY.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.XSD_BASE_64_BINARY.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.XSD_ANY_URI.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.XSD_DATE_TIME.getIRI()));
		Dts.add(axiomFactory.getOWLDataFactory().getOWLDatatype(OWL2Datatype.XSD_DATE_TIME_STAMP.getIRI()));

		return d.recComputation(axiomFactory.getOWLDataFactory().getOWLTopDataProperty(), Dts);
	}

	public static Collection<OWLAxiom> compute (HierarchyBuilder hierarchyBuilder, AxiomFactory axiomFactory, OWLReasoner reason, Collection<OWLDatatype> Dts) {
		DataPropertyRangeAxioms d = new DataPropertyRangeAxioms(hierarchyBuilder, axiomFactory, reason);
		return d.recComputation(axiomFactory.getOWLDataFactory().getOWLTopDataProperty(), Dts);
	}

	private Collection<OWLAxiom> recComputation (OWLDataProperty U, Collection<OWLDatatype> Dts) {
		Collection<OWLAxiom> ret = new HashSet<OWLAxiom>();
		if (!U.isTopEntity() && !U.isBottomEntity())
			for (OWLDatatype DT : Dts) {
				OWLAxiom a = this.axiomFactory.getOWLDataFactory().getOWLDataPropertyRangeAxiom(U, DT);
				if (this.reason.isEntailed(a))
					ret.add(a);
			}
		Collection<OWLDataProperty> Ucs = this.hierarchyBuilder.getChildrenOf(U, true);
		for (OWLDataProperty Uc : Ucs)
			ret.addAll(this.recComputation(Uc, Dts));
		return ret;
	}
}