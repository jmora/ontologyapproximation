package eu.optique.api.component.omm.approximators.runners;

import java.io.File;
import java.net.URI;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.HermiT.Reasoner.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public class ConcurrentComparator implements Callable<String> {

	static private Logger logger = Logger.getLogger(ConcurrentComparator.class.getName());
	static private ReasonerFactory reasonerFactory = new Reasoner.ReasonerFactory();
	private String name;
	private OWLOntology syntacticApproximation;
	private OWLOntology localApproximation;
	private OWLOntology globalApproximation;
	private String syntacticApproximationPath;
	private String localApproximationPath;
	private String globalApproximationPath;

	public ConcurrentComparator(String name, String syntacticApproximation, String localApproximation, String globalApproximation) throws OWLOntologyCreationException {
		this.name = name;
		this.syntacticApproximationPath = syntacticApproximation;
		this.localApproximationPath = localApproximation;
		this.globalApproximationPath = globalApproximation;
	}

	private void init () throws OWLOntologyCreationException {
		this.syntacticApproximation = this.getOntology(this.syntacticApproximationPath);
		this.localApproximation = this.getOntology(this.localApproximationPath);
		this.globalApproximation = this.getOntology(this.globalApproximationPath);
	}

	private OWLOntology getOntology (String ontologyPath) throws OWLOntologyCreationException {
		File ontologyFile = new File(URI.create(ontologyPath));
		return OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(ontologyFile);
	}

	public String call () throws OWLOntologyCreationException {
		ConcurrentComparator.logger.info("Started " + this.name);
		String result = this.name;
		this.init();
		result += "\t" + this.getAxiomCount(this.syntacticApproximation);
		result += "\t" + this.getAxiomCount(this.localApproximation);
		result += "\t" + this.getAxiomCount(this.globalApproximation);
		result += "\t" + this.entails(this.syntacticApproximation, this.localApproximation);
		result += "\t" + this.entails(this.syntacticApproximation, this.globalApproximation);
		result += "\t" + this.entails(this.localApproximation, this.syntacticApproximation);
		result += "\t" + this.entails(this.globalApproximation, this.syntacticApproximation);
		ConcurrentComparator.logger.info("Finished " + this.name);
		return result;
	}

	private String getAxiomCount (OWLOntology ontology) {
		return new Integer(ontology.getLogicalAxioms().size()).toString();
	}

	private String entails (OWLOntology ontology1, OWLOntology ontology2) {
		try {
			OWLReasoner reasoner = ConcurrentComparator.reasonerFactory.createReasoner(ontology1);
			reasoner.precomputeInferences();
			int entailed = 0;
			for (OWLAxiom axiom : ontology2.getLogicalAxioms())
				if (reasoner.isEntailed(axiom))
					entailed++;
			return new Integer(entailed).toString();
		} catch (Exception e) {
			return "exception";
		}
	}

	public String getName () {
		return this.name;
	}

}
