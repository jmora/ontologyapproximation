package eu.optique.api.component.omm.approximators.runners;

import java.io.File;

import org.apache.log4j.Logger;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.HermiT.Reasoner.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public class ApproximatorComparator {

	static private ReasonerFactory reasonerFactory = new Reasoner.ReasonerFactory();
	static private Logger logger = Logger.getLogger(ApproximatorComparator.class.getName());

	public static void main (String[] args) throws OWLOntologyCreationException {
		OWLOntology globalOntology = OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(new File(args[0]));
		OWLOntology localOntology = OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(new File(args[1]));
		int entailed = 0;
		OWLReasoner reasoner = ApproximatorComparator.reasonerFactory.createReasoner(localOntology);
		for (OWLAxiom axiom : globalOntology.getLogicalAxioms())
			if (reasoner.isEntailed(axiom))
				entailed++;
		ApproximatorComparator.logger.info("Entailment of global approximation by local in axioms: " + entailed);
		if (args.length > 2) {
			OWLOntology originalOntology = OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(new File(args[2]));
			ApproximatorComparator.testRelation(localOntology, globalOntology, originalOntology);
		}
	}

	private static void testRelation (OWLOntology localOntology, OWLOntology globalOntology, OWLOntology originalOntology) {
		OWLReasoner localReasoner = ApproximatorComparator.reasonerFactory.createReasoner(localOntology);
		OWLReasoner globalReasoner = ApproximatorComparator.reasonerFactory.createReasoner(globalOntology);
		for (OWLAxiom axiom : originalOntology.getLogicalAxioms())
			if (localReasoner.isEntailed(axiom) && !globalReasoner.isEntailed(axiom))
				ApproximatorComparator.logger.debug("Problem detected: Axiom entailed by local and not by global approach; " + axiom.toString());
	}
}
