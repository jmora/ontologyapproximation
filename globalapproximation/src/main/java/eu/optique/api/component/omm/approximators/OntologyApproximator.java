package eu.optique.api.component.omm.approximators;

import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public interface OntologyApproximator {

	OWLOntology approximate (OWLOntology sourceOntology, OWLReasonerFactory reasonerFactory) throws OWLOntologyCreationException;

}
