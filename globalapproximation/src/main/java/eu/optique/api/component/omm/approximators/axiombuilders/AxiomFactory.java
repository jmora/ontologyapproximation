package eu.optique.api.component.omm.approximators.axiombuilders;

import java.util.Collection;
import java.util.HashSet;

import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;

import eu.optique.api.component.omm.approximators.preprocess.HierarchyBuilder;
import eu.optique.api.component.omm.approximators.preprocess.NameManager;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public class AxiomFactory {
	private NameManager nameManager;
	private OWLDataFactory factory;

	public AxiomFactory(NameManager m, OWLDataFactory f) {
		this.nameManager = m;
		this.factory = f;
	}

	public NameManager getNameManager () {
		return this.nameManager;
	}

	public OWLDataFactory getOWLDataFactory () {
		return this.factory;
	}

	public boolean isArtificial (OWLClass owlClass) {
		return this.nameManager.isArtificial(owlClass);
	}

	public Collection<OWLAxiom> subAxioms (OWLClassExpression parentC, Collection<OWLClass> Cs) {
		Collection<OWLAxiom> ret = new HashSet<OWLAxiom>();
		for (OWLClass cc : Cs)
			this.addSubAxiom(ret, this.nameManager.getExpressionFromName(cc), parentC);

		return ret;
	}

	public Collection<OWLAxiom> subAxioms (OWLObjectPropertyExpression parentP, Collection<OWLObjectPropertyExpression> Ps) {
		Collection<OWLAxiom> ret = new HashSet<OWLAxiom>();
		for (OWLObjectPropertyExpression P : Ps)
			this.addSubAxiom(ret, P, parentP);
		return ret;
	}

	public Collection<OWLAxiom> subAxioms (OWLDataPropertyExpression parentU, Collection<OWLDataPropertyExpression> Us) {
		Collection<OWLAxiom> ret = new HashSet<OWLAxiom>();
		for (OWLDataPropertyExpression U : Us)
			this.addSubAxiom(ret, U, parentU);
		return ret;
	}

	public Collection<OWLAxiom> buildDisjointnessAxioms (OWLClass C, Collection<OWLClass> Ds) {
		Collection<OWLAxiom> ret = new HashSet<OWLAxiom>();
		if (C.isBottomEntity() || C.isTopEntity())
			return ret;
		OWLClassExpression Ct = this.nameManager.getExpressionFromName(C);
		for (OWLClass D : Ds)
			this.addDisjAxiom(ret, Ct, this.nameManager.getExpressionFromName(D));
		return ret;
	}

	public Collection<OWLAxiom> buildDisjointnessAxioms (OWLObjectPropertyExpression P, Collection<OWLObjectPropertyExpression> Ds) {
		Collection<OWLAxiom> ret = new HashSet<OWLAxiom>();
		if (P.isBottomEntity() || P.isTopEntity())
			return ret;
		for (OWLObjectPropertyExpression D : Ds)
			this.addDisjAxiom(ret, P, D);
		return ret;
	}

	public Collection<OWLAxiom> buildDisjointnessAxioms (OWLDataProperty U, Collection<OWLDataProperty> Ds) {
		Collection<OWLAxiom> ret = new HashSet<OWLAxiom>();
		if (U.isBottomEntity() || U.isTopEntity())
			return ret;
		for (OWLDataProperty D : Ds)
			this.addDisjAxiom(ret, U, D);
		return ret;
	}

	public Collection<OWLAxiom> buildAxiomsFromHierarchies (HierarchyBuilder b) {
		Collection<OWLAxiom> ret = new HashSet<OWLAxiom>();
		ret.addAll(this.recBuildAxiomsFromHierarchies(this.factory.getOWLThing(), b));
		ret.addAll(this.recBuildAxiomsFromHierarchies(this.factory.getOWLTopObjectProperty(), b));
		ret.addAll(this.recBuildAxiomsFromHierarchies(this.factory.getOWLTopDataProperty(), b));
		return ret;
	}

	private Collection<OWLAxiom> recBuildAxiomsFromHierarchies (OWLClass parentC, HierarchyBuilder b) {
		Collection<OWLAxiom> ret = new HashSet<OWLAxiom>();
		Collection<OWLClass> Cs = b.getChildrenOf(parentC, true);
		for (OWLClass C : Cs)
			ret.addAll(this.recBuildAxiomsFromHierarchies(C, b));
		for (OWLClass C : Cs)
			this.addSubAxiom(ret, this.nameManager.getExpressionFromName(C), this.nameManager.getExpressionFromName(parentC));
		return ret;
	}

	private Collection<OWLAxiom> recBuildAxiomsFromHierarchies (OWLObjectPropertyExpression parentP, HierarchyBuilder b) {
		Collection<OWLAxiom> ret = new HashSet<OWLAxiom>();
		Collection<OWLObjectPropertyExpression> Cs = b.getChildrenOf(parentP, true);
		for (OWLObjectPropertyExpression C : Cs)
			ret.addAll(this.recBuildAxiomsFromHierarchies(C, b));
		for (OWLObjectPropertyExpression C : Cs)
			this.addSubAxiom(ret, C, parentP);
		return ret;
	}

	private Collection<OWLAxiom> recBuildAxiomsFromHierarchies (OWLDataProperty parentU, HierarchyBuilder b) {
		Collection<OWLAxiom> ret = new HashSet<OWLAxiom>();
		Collection<OWLDataProperty> Cs = b.getChildrenOf(parentU, true);
		for (OWLDataProperty C : Cs)
			ret.addAll(this.recBuildAxiomsFromHierarchies(C, b));
		for (OWLDataProperty C : Cs)
			this.addSubAxiom(ret, C, parentU);
		return ret;
	}

	private void addSubAxiom (Collection<OWLAxiom> c, OWLClassExpression child, OWLClassExpression parent) {
		if (!child.isBottomEntity() && !parent.isTopEntity())
			c.add(this.factory.getOWLSubClassOfAxiom(child, parent));
	}

	private void addSubAxiom (Collection<OWLAxiom> c, OWLObjectPropertyExpression child, OWLObjectPropertyExpression parent) {
		if (!child.isBottomEntity() && !parent.isTopEntity())
			c.add(this.factory.getOWLSubObjectPropertyOfAxiom(child, parent));
	}

	private void addSubAxiom (Collection<OWLAxiom> c, OWLDataPropertyExpression child, OWLDataPropertyExpression parent) {
		if (!child.isBottomEntity() && !parent.isTopEntity())
			c.add(this.factory.getOWLSubDataPropertyOfAxiom(child, parent));
	}

	private void addDisjAxiom (Collection<OWLAxiom> c, OWLClassExpression child, OWLClassExpression parent) {
		if (!child.isBottomEntity() && !parent.isBottomEntity())
			if (!child.equals(parent))
				c.add(this.factory.getOWLDisjointClassesAxiom(child, parent));
			else
				c.add(this.factory.getOWLEquivalentClassesAxiom(child, this.factory.getOWLNothing()));
	}

	private void addDisjAxiom (Collection<OWLAxiom> c, OWLObjectPropertyExpression child, OWLObjectPropertyExpression parent) {
		if (!child.isBottomEntity() && !parent.isBottomEntity())
			if (!child.equals(parent))
				c.add(this.factory.getOWLDisjointObjectPropertiesAxiom(child, parent));
			else
				c.add(this.factory.getOWLEquivalentObjectPropertiesAxiom(child, this.factory.getOWLBottomObjectProperty()));
	}

	private void addDisjAxiom (Collection<OWLAxiom> c, OWLDataPropertyExpression child, OWLDataPropertyExpression parent) {
		if (!child.isBottomEntity() && !parent.isBottomEntity())
			if (!child.equals(parent))
				c.add(this.factory.getOWLDisjointDataPropertiesAxiom(child, parent));
			else
				c.add(this.factory.getOWLEquivalentDataPropertiesAxiom(child, this.factory.getOWLBottomDataProperty()));
	}

	public Collection<OWLAxiom> getEquivalencesC (HashSet<HashSet<OWLClass>> eqs) {
		Collection<OWLAxiom> ret = new HashSet<OWLAxiom>();
		boolean badset = false;
		for (HashSet<OWLClass> e : eqs) {
			HashSet<OWLClassExpression> renE = new HashSet<OWLClassExpression>();
			badset = false;
			for (OWLClass C : e) {
				OWLClassExpression expression = this.nameManager.getExpressionFromName(C);
				renE.add(expression);
				badset |= expression.isBottomEntity() || expression.isTopEntity();
			}
			if (renE.size() > 1 && !badset)
				ret.add(this.factory.getOWLEquivalentClassesAxiom(renE));
		}
		return ret;
	}

	public Collection<OWLAxiom> getEquivalencesP (HashSet<HashSet<OWLObjectPropertyExpression>> eqs) {
		Collection<OWLAxiom> ret = new HashSet<OWLAxiom>();
		boolean badset;
		for (HashSet<OWLObjectPropertyExpression> e : eqs) {
			badset = false;
			for (OWLObjectPropertyExpression objectPropertyExpression : e)
				badset |= objectPropertyExpression.isTopEntity() || objectPropertyExpression.isBottomEntity();
			if (!badset)
				ret.add(this.factory.getOWLEquivalentObjectPropertiesAxiom(e));
		}
		return ret;
	}

	public Collection<OWLAxiom> getEquivalencesU (HashSet<HashSet<OWLDataProperty>> eqs) {
		Collection<OWLAxiom> ret = new HashSet<OWLAxiom>();
		boolean badset;
		for (HashSet<OWLDataProperty> e : eqs) {
			badset = false;
			for (OWLDataProperty dataProperty : e)
				badset |= dataProperty.isTopEntity() || dataProperty.isBottomEntity();
			if (!badset)
				ret.add(this.factory.getOWLEquivalentDataPropertiesAxiom(e));
		}
		return ret;
	}
}
