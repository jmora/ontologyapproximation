package eu.optique.api.component.omm.approximators.axiombuilders;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import eu.optique.api.component.omm.approximators.preprocess.HierarchyBuilder;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public class EquivalenceAxioms {
	private HierarchyBuilder hierarchyBuilder;
	private AxiomFactory axiomFactory;
	private OWLReasoner reasoner;

	public EquivalenceAxioms(HierarchyBuilder hierarchyBuilder, AxiomFactory axiomFactory, OWLReasoner reasoner) {
		this.hierarchyBuilder = hierarchyBuilder;
		this.axiomFactory = axiomFactory;
		this.reasoner = reasoner;
	}

	public ArrayList<OWLAxiom> getAxioms () {
		ArrayList<OWLAxiom> axioms = new ArrayList<OWLAxiom>();
		HashSet<HashSet<OWLClass>> equivalencesC = new HashSet<HashSet<OWLClass>>();
		Set<OWLClass> Cs = this.reasoner.getRootOntology().getClassesInSignature();
		for (OWLClass C : Cs) {
			HashSet<OWLClass> eqs = new HashSet<OWLClass>();
			eqs.addAll(this.reasoner.getEquivalentClasses(C).getEntities());
			eqs.add(C);
			equivalencesC.add(eqs);
		}

		HashSet<HashSet<OWLObjectPropertyExpression>> equivalencesP = new HashSet<HashSet<OWLObjectPropertyExpression>>();
		Set<OWLObjectProperty> Ps = this.reasoner.getRootOntology().getObjectPropertiesInSignature();
		for (OWLObjectProperty P : Ps) {
			HashSet<OWLObjectPropertyExpression> eqs = new HashSet<OWLObjectPropertyExpression>();
			HashSet<OWLObjectPropertyExpression> invEqs = new HashSet<OWLObjectPropertyExpression>();

			eqs.addAll(this.reasoner.getEquivalentObjectProperties(P).getEntities());
			eqs.add(P);
			if (eqs.size() > 1)
				equivalencesP.add(eqs);

			invEqs.addAll(this.reasoner.getEquivalentObjectProperties(P.getInverseProperty()).getEntities());
			invEqs.add(P.getInverseProperty());
			if (invEqs.size() > 1)
				equivalencesP.add(invEqs);
		}

		HashSet<HashSet<OWLDataProperty>> equivalencesU = new HashSet<HashSet<OWLDataProperty>>();
		Set<OWLDataProperty> Us = this.reasoner.getRootOntology().getDataPropertiesInSignature();
		for (OWLDataProperty U : Us)
			if (U.isTopEntity()) {
				HashSet<OWLDataProperty> eqs = new HashSet<OWLDataProperty>();
				eqs.addAll(this.reasoner.getEquivalentDataProperties(U).getEntities());
				eqs.add(U);
				if (eqs.size() > 1)
					equivalencesU.add(eqs);
			}

		axioms.addAll(this.axiomFactory.getEquivalencesC(equivalencesC));
		axioms.addAll(this.axiomFactory.getEquivalencesP(equivalencesP));
		axioms.addAll(this.axiomFactory.getEquivalencesU(equivalencesU));
		return axioms;
	}

}
