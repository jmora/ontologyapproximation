package eu.optique.api.component.omm.approximators;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.profiles.OWL2QLProfile;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;

import eu.optique.api.component.omm.approximators.axiombuilders.AxiomFactory;
import eu.optique.api.component.omm.approximators.axiombuilders.DataPropertyRangeAxioms;
import eu.optique.api.component.omm.approximators.axiombuilders.Disjointnesses;
import eu.optique.api.component.omm.approximators.axiombuilders.EquivalenceAxioms;
import eu.optique.api.component.omm.approximators.axiombuilders.ObjectPropertyAxioms;
import eu.optique.api.component.omm.approximators.preprocess.HierarchyBuilder;
import eu.optique.api.component.omm.approximators.preprocess.NameManager;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public class OntologyGlobalApproximator extends AbstractOntologyApproximator {

	static private Logger logger = Logger.getLogger(OntologyGlobalApproximator.class.getName());
	private OWLDataFactory factory = OWLManager.getOWLDataFactory();

	public OWLOntology approximate (OWLOntology sourceOntology, OWLReasonerFactory reasonerFactory) throws OWLOntologyCreationException {
		OntologyGlobalApproximator.logger.trace("Starting approximation");
		if (new OWL2QLProfile().checkOntology(sourceOntology).isInProfile()) {
			OntologyGlobalApproximator.logger.trace("Done, saving");
			OntologyGlobalApproximator.logger.trace("Problem detected: ontology already in the OWL2 QL profile");
			return sourceOntology;
		}
		NameManager nameManager = new NameManager(sourceOntology);
		OWLOntology namedOntology = nameManager.getNamedOntology();
		AxiomFactory axiomFactory = new AxiomFactory(nameManager, this.factory);
		OWLReasoner reasoner = reasonerFactory.createReasoner(namedOntology);
		OntologyGlobalApproximator.logger.trace("Precomputing inferences");
		reasoner.precomputeInferences();
		OntologyGlobalApproximator.logger.trace("Precomputing hierarchies");
		HierarchyBuilder hierarchies = new HierarchyBuilder(namedOntology, reasoner);
		OntologyGlobalApproximator.logger.trace("Obtaining axioms");
		Set<OWLAxiom> axioms = this.innerApproximate(hierarchies, axiomFactory, reasoner);
		OWLOntology targetOntology = this.createOntologyFromAxiomSet(axioms, sourceOntology);
		return targetOntology;
	}

	private Set<OWLAxiom> innerApproximate (HierarchyBuilder hierarchies, AxiomFactory axiomFactory, OWLReasoner reasoner) {
		Set<OWLAxiom> axioms = new HashSet<OWLAxiom>();
		// OntologyGlobalApproximator.logger.trace("Going for existentials");
		axioms.addAll(new ExistentialExplorer(hierarchies, axiomFactory).getAxioms());
		// OntologyGlobalApproximator.logger.trace("Going for disjointness");
		// axioms.addAll(ExistentialsChildren.compute(hierarchies, axiomFactory));
		axioms.addAll(new Disjointnesses(hierarchies, axiomFactory).compute());
		// axioms.addAll(disjTmp.splitDisjoint(reasoner.getRootOntology(), this.factory));
		// OntologyGlobalApproximator.logger.trace("Going properties properties");
		axioms.addAll(ObjectPropertyAxioms.compute(hierarchies, axiomFactory, reasoner));
		axioms.addAll(DataPropertyRangeAxioms.compute(hierarchies, axiomFactory, reasoner));
		axioms.addAll(DataPropertyRangeAxioms.compute(hierarchies, axiomFactory, reasoner));
		// OntologyGlobalApproximator.logger.trace("Going for hierarchies");
		axioms.addAll(axiomFactory.buildAxiomsFromHierarchies(hierarchies));
		axioms.addAll(new EquivalenceAxioms(hierarchies, axiomFactory, reasoner).getAxioms());
		return axioms;
	}

	public Set<OWLAxiom> approximate (Set<OWLAxiom> axioms, OWLReasonerFactory reasonerFactory) throws OWLOntologyCreationException {
		NameManager nameManager = new NameManager(axioms);
		OWLOntology namedOntology = nameManager.getNamedOntology();
		OWLReasoner reasoner = reasonerFactory.createReasoner(namedOntology);
		reasoner.precomputeInferences();
		return this.innerApproximate(new HierarchyBuilder(namedOntology, reasoner), new AxiomFactory(nameManager, this.factory), reasoner);
	}
}
