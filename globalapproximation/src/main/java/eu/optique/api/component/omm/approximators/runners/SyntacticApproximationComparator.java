package eu.optique.api.component.omm.approximators.runners;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

/**
 * @author Marco Console and Jose Mora
 * 
 */
public class SyntacticApproximationComparator {

	private static final ExecutorService pool = Executors.newFixedThreadPool(8);
	private static PrintStream output;
	static private Logger logger = Logger.getLogger(SyntacticApproximationComparator.class.getName());

	public static void main (String[] args) throws InterruptedException, ExecutionException, OWLOntologyCreationException, FileNotFoundException {
		String basepath;
		if (args.length > 0)
			basepath = new File(args[0]).toURI().toString();
		else
			basepath = "file:///D:/programs/scala-eclipse/workspace/approximation.syntactic.comparison/ontologies/";
		SyntacticApproximationComparator.output = new PrintStream(new File("results.txt"));
		ArrayList<Future<String>> results = new ArrayList<Future<String>>();
		ArrayList<ConcurrentComparator> testcases = SyntacticApproximationComparator.getTestCases(basepath);
		for (ConcurrentComparator t : testcases)
			results.add(SyntacticApproximationComparator.pool.submit(t));
		int index = 0;
		for (Future<String> r : results) {
			SyntacticApproximationComparator.output.println(SyntacticApproximationComparator.maybeGet(r, testcases.get(index).getName()));
			index++;
		}

	}

	private static String maybeGet (Future<String> r, String caseName) throws InterruptedException, ExecutionException {
		return r.get();
	}

	// no timeout, but maybe some other time...
	// private static String maybeGet (Future<String> r, String caseName) throws InterruptedException, ExecutionException {
	// try {
	// return r.get(60, TimeUnit.MINUTES);
	// } catch (TimeoutException e) {
	// if (!r.isDone()) {
	// r.cancel(true);
	// SyntacticApproximationComparator.logger.info("Cancelled " + caseName);
	// }
	// return caseName + "\ttimeout";
	// }
	// }

	private static ArrayList<ConcurrentComparator> getTestCases (String basepath) throws OWLOntologyCreationException {
		File syntacticList = new File(java.net.URI.create(basepath + "syntactic/"));
		ArrayList<ConcurrentComparator> testcases = new ArrayList<ConcurrentComparator>();
		for (File sap : syntacticList.listFiles())
			testcases.add(SyntacticApproximationComparator.getTestCase(sap, basepath));
		return testcases;
	}

	private static ConcurrentComparator getTestCase (File sap, String basepath) throws OWLOntologyCreationException {
		String syntacticApproximationName = sap.getName();
		String originalName = syntacticApproximationName.substring(0, syntacticApproximationName.length() - 11);
		String localApproximationName = originalName + "localapx.owl";
		String globalApproximationName = originalName + "globalapx.owl";
		return new ConcurrentComparator(originalName, sap.toURI().toString(), basepath + "semantic/" + localApproximationName, basepath + "semantic/" + globalApproximationName);
	}

}
